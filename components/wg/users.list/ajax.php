<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\UI\PageNavigation as Nav;
use \Bitrix\Main\UserTable as User;
use Bitrix\Main\Engine\Controller;
 
class CustomAjaxController extends Controller
{
	/**
	 * @return array
	 */
	public function configureActions()
	{
		return [
			'getUsers' => [
				'prefilters' => []
			]
		];
	}
 
	/**
	 * @param string $page
	 * @param string $limit
	 * @return array
	 */
	public static function getUsersAction($page = 0, $limit = 10)
	{
        $offset = ($page != 0 ? $page - 1 : 0) * $limit;
        $userList = User::getList(
            array(
                "select" => Array("ID","NAME","LOGIN", "LAST_NAME"),
                "filter" => Array(),
                "count_total" => true,
                "offset" => $offset,
                "limit" => $limit,
            )
        );

		return [
			'users' => $userList->fetchAll(),
            'limit' => $limit,
            'offset' => $offset,
            'page' => $page
        ];
	}
 
}