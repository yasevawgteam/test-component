<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 

$arComponentDescription = array(
        "NAME" => GetMessage("Список пользователей"),
        "DESCRIPTION" => GetMessage("Выводим список пользователей"),
        "PATH" => array(
            "ID" => "wg_components",
            "NAME" => "Компоненты WG",
            "CHILD" => array(
                "ID" => "userlist",
                "NAME" => "Список пользователей"
            )
        ),
    "ICON" => "",
);
?>