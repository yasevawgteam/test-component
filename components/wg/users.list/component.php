<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\UserTable as User;
use \Bitrix\Main\UI\PageNavigation as Nav;
    

$arResult['page'] = ($_REQUEST["page"] && $_REQUEST["page"] != 0) ? ($_REQUEST["page"]) : 0;
$arResult['limit'] = 10;
$arResult["offset"] = ($arResult['page'] - 1) * $arResult['limit'];

$arResult['users'] = User::getList(
        Array(
            "select" => Array("ID","NAME","LOGIN", "LAST_NAME"),
            "filter" => Array(),
            "limit" => $arResult['limit'],
            "offset" => $arResult["offset"]
        )
    )
    ->fetchAll();


$this->IncludeComponentTemplate();
?>