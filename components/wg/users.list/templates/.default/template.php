<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<table id="users">
    <thead>
        <th>№</th>
        <th>NAME</th>
        <th>LAST_NAME</th>
        <th>LOGIN</th>
        <th>ID</th>
    </thead>
    <tbody id="tbody_users">
    <?foreach($arResult['users'] as $key => $user):?>
        <tr>
            <td><?=$arResult['offset'] + $key + 1?></td>
            <td><?=$user['NAME']?></td>
            <td><?=$user['LAST_NAME']?></td>
            <td><?=$user['LOGIN']?></td>
            <td><?=$user['ID']?></td>
        </tr>
    <?endforeach?>
    </tbody>
</table>

<nav>
  <ul class="pagination" id="pagination">
    <li class="page-item" <?=$arResult['page'] == 1 ? 'style="font-weight: bold"' : ""?>><a class="page-link" href="#"><?=$arResult['page'] != 1 ? $arResult['page'] - 1 : 1 ?></a></li>
    <li class="page-item" <?=$arResult['page'] != 1 ? 'style="font-weight: bold"' : ""?>><a class="page-link" href="#"><?=$arResult['page'] != 1 ? $arResult['page'] : 2 ?></a></li>
    <li class="page-item"><a class="page-link" href="#"><?=$arResult['page'] != 1 ? $arResult['page'] + 1 : 3 ?></a></li>
  </ul>
</nav>