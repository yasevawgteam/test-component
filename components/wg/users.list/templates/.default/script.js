
window.onload = function(){
    const 
        targetLink = document.querySelector(".pagination"),
        tbody = document.getElementById("tbody_users"),
        pagination = document.getElementById("pagination")

    targetLink.addEventListener("click", (e) => {
        BX.showWait()
        e.preventDefault()
        
        let 
            page = parseInt(e.target.innerText),
            limit = 10,
            offset = (page != 0 ? page - 1 : 0) * limit

        let request = BX.ajax.runComponentAction('wg:users.list', 'getUsers', {
                mode:'ajax',
                data: {
                    page: page,
                    limit: limit
                }
            })
        
        request.then(function(response){
            BX.closeWait()

            window.history.pushState({ page: page }, `Пользователи`, `?page=${page}`)
            tbody.innerHTML = getTableHTML(response.data, offset)
            pagination.innerHTML = getPaginationHTML(page)
        })
    })

    function getTableHTML(data, offset){
        return data.users.map((user, index) => {
            return `
                <tr>
                    <td>${offset + index + 1}</td>
                    <td>${user['NAME']}</td>
                    <td>${user['LAST_NAME']}</td>
                    <td>${user['LOGIN']}</td>
                    <td>${user['ID']}</td>
                </tr>
            `
        }).join("")
    }

    function getPaginationHTML($page){
        return `
            <li class="page-item"><a class="page-link" ${$page === 1 ? 'style="font-weight: bold"' : ""} href="#">${$page !== 1 ? $page - 1 : 1}</a></li>
            <li class="page-item"><a class="page-link" ${$page !== 1 ? 'style="font-weight: bold"' : ""} href="#">${$page !== 1 ? $page : 2}</a></li>
            <li class="page-item"><a class="page-link" href="#">${$page !== 1 ? $page + 1 : 3}</a></li>
        `
    }

    function getParam(name){
        if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
           return decodeURIComponent(name[1])
    }
}